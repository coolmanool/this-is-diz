var openDisqus = function openDisqus() {
    var $disqus = $(".disqus");
    if ($disqus.css("display") === "none") {
        $disqus.css("display", "block");
    } else {
        $disqus.css("display", "none");
    }
};

$(function() {
    /* Correct wrappers size */
    $(".wrapper").each(function(index) {
        var $child = $(this).find(".overwrapper"),
            height = $child.outerHeight(),
            width = $child.outerWidth();
        if ($(this).hasClass("wrapper-mark")) {
            $(this).css({"height": height - 4, "width": width});
        } else {
            $(this).css({"height": height, "width": width});
        }
    });

    /* Correct big quotes size */
    var height = $(".quote--big__content").outerHeight();
    $(".quote--big__icon-block").css("height", height);
    $(".wrapper--quote-big-icon").css({"width": 130, "margin-top": height / 2 - 70});


    $(".wrapper--comments").on("click", openDisqus);

    /*TEST*/
    $(".layer--top").each(function(index) {
        var $this = $(this),
            $child = $this.find(".layer--bottom"),
            height = $this.outerHeight(),
            width = $this.outerWidth();
        $child.css({"height": height, "width": width})
    })
});